# PasswdAsAService-ChaseLabar

1. To install dependencies, run "pip install -r requirements.txt"
2. To run service, run "python app.py"
3. To run unit tests, run "python -m unittest -v unit_tests.UnitTests"
4. To set paths to users/groups, modify USERS_PATH and GROUPS_PATH variables in app.py