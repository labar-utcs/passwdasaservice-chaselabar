import unittest


class UnitTests(unittest.TestCase):
    def test_get_groups(self):
        from app import get_groups, set_group_path
        set_group_path("test_groups")
        self.assertDictEqual(get_groups()[1], {"gid": 220, "name": '_devicemgr', "members": []})

    def test_get_users(self):
        from app import get_users, set_user_path
        set_user_path("test_users")
        self.assertDictEqual(get_users()[1], {"gid": 94, "uid": 94, "name": '_teamsserver',
                                              "comment": 'TeamsServer', "home": '/var/teamsserver',
                                              "shell": '/usr/bin/false'})

    def test_group_filters(self):
        from app import filter_groups, get_groups, set_group_path
        set_group_path("test_groups")
        groups = get_groups()
        target_result = {"gid": 220, "name": '_devicemgr', "members": []}
        self.assertDictEqual(filter_groups(groups, gid=220)[0], target_result)
        self.assertDictEqual(filter_groups(groups, name='_devicemgr')[0], target_result)
        self.assertTrue(len(filter_groups(groups, gid=9999)) < 1)

    def test_user_filters(self):
        from app import filter_users, get_users, set_user_path
        set_user_path("test_users")
        users = get_users()
        target_result = {"gid": 94, "uid": 94, "name": '_teamsserver',
                         "comment": 'TeamsServer', "home": '/var/teamsserver',
                         "shell": '/usr/bin/false'}
        self.assertDictEqual(filter_users(users, gid=94)[0], target_result)
        self.assertDictEqual(filter_users(users, name='_teamsserver')[0], target_result)
        self.assertDictEqual(filter_users(users, comment='TeamsServer')[0], target_result)
        self.assertDictEqual(filter_users(users, home='/var/teamsserver')[0], target_result)
        self.assertDictEqual(filter_users(users, shell='/usr/bin/false')[0], target_result)
        self.assertTrue(len(filter_users(users, gid=9999)) < 1)
