import flask
import os
from flask import jsonify, Flask, redirect, url_for, request, abort

application = Flask(__name__)
app = application

USERS_PATH = "/etc/passwd"
GROUPS_PATH = "/etc/group"


@app.route("/groups", methods=['GET'])
def groups_request():
    return jsonify(get_groups())


@app.route("/groups/<gid>", methods=['GET'])
def group_gid_request(gid):
    groups = get_groups()
    filtered_groups = filter_groups(groups, gid=str(gid))
    if len(filtered_groups) < 1:
        abort(404)
    return jsonify(filtered_groups[0])


@app.route("/groups/query", methods=['GET'])
def group_query_request():
    groups = get_groups()
    name = request.args.get('name')
    gid = request.args.get('gid')
    members = request.args.getlist('member')
    return jsonify(filter_groups(groups, name=name, members=members, gid=gid))


@app.route("/users", methods=['GET'])
def users_request():
    return jsonify(get_users())


@app.route("/users/<uid>", methods=['GET'])
def user_uid_request(uid):
    users = get_users()
    filtered_users = filter_users(users, uid=str(uid))
    if len(filtered_users) < 1:
        abort(404)
    return jsonify(filtered_users[0])


@app.route("/users/<uid>/groups", methods=['GET'])
def user_groups_request(uid):
    users = get_users()
    chosen_user = filter_users(users, uid=str(uid))[0]
    groups = get_groups()
    return jsonify(filter_groups(groups, members=[chosen_user["name"]]))


@app.route("/users/query", methods=['GET'])
def user_query_request():
    users = get_users()
    name = request.args.get('name')
    uid = request.args.get('uid')
    gid = request.args.get('gid')
    comment = request.args.get('comment')
    home = request.args.get('home')
    shell = request.args.get('shell')
    return jsonify(filter_users(users, name=name, uid=uid, gid=gid,
                                comment=comment, home=home, shell=shell))


# return all groups present
def get_groups():
    try:
        group_data = get_entries_from_filepath(GROUPS_PATH)
        groups = []
        for group in group_data:
            group_dict = {"name": group[0], "gid": int(group[2])}
            members = []
            # need to split member section into individual entries, remove \n tokens, filter empty strings
            for member in group[3].split(','):
                member = str(member).rstrip('\n')
                if len(member) > 0:
                    members.append(member)
            group_dict["members"] = members
            groups.append(group_dict)
        return remove_duplicates(groups)
    except:
        abort(500)


# return all users present
def get_users():
    try:
        user_data = get_entries_from_filepath(USERS_PATH)
        users = []
        for user in user_data:
            user_dict = {"name": user[0], "uid": int(user[2]), "gid": int(user[3]),
                         "comment": user[4], "home": user[5], "shell": user[6]}
            users.append(user_dict)
        return remove_duplicates(users)
    except:
        abort(500)


# remove duplicate entries from lists
def remove_duplicates(a_list):
    result = []
    for entry in a_list:
        if entry not in result:
            result.append(entry)
    return result


# filter group list from get_groups based on given parameters
def filter_groups(groups, name=None, gid=None, members=None):
    result_groups = []
    for group in groups:
        matches_query = True

        if (name is not None) and (str(group["name"]) != str(name)):
            matches_query = False
        if (gid is not None) and (str(group["gid"]) != str(gid)):
            matches_query = False
        if members is not None:
            for member in members:
                if str(member) not in group["members"]:
                    matches_query = False

        if matches_query:
            result_groups.append(group)
    return result_groups


# filter user list from get_users based on given parameters
def filter_users(users, name=None, uid=None, gid=None, comment=None, home=None, shell=None):
    result_users = []
    for user in users:
        matches_query = True

        if (name is not None) and (str(user["name"]) != str(name)):
            matches_query = False
        if (uid is not None) and (str(user["uid"]) != str(uid)):
            matches_query = False
        if (gid is not None) and (str(user["gid"]) != str(gid)):
            matches_query = False
        if (comment is not None) and (str(user["comment"]) != str(comment)):
            matches_query = False
        if (home is not None) and (str(user["home"]) != str(home)):
            matches_query = False
        if (shell is not None) and (str(user["shell"]) != str(shell)):
            matches_query = False

        if matches_query:
            result_users.append(user)
    return result_users


# return all users/groups present in targeted file
def get_entries_from_filepath(path):
    converted_path = os.path.expanduser(path)
    with open(converted_path) as fp:
        current_line = fp.readline()
        entries = []
        while current_line:
            split_line = current_line.split(':')
            if len(split_line) > 2:
                entries.append(split_line)
            current_line = fp.readline()
        return entries


def set_group_path(new_path):
    global GROUPS_PATH
    GROUPS_PATH = new_path


def set_user_path(new_path):
    global USERS_PATH
    USERS_PATH = new_path


if __name__ == "__main__":
    app.debug = False
    app.run(host='0.0.0.0', port=80)
